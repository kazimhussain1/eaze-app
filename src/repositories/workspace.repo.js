const WorkSpace = require("../models/WorkSpace");
const User = require('../models/User')

module.exports = {
    async createWorkSpace(req, res) {

        try {

            console.log('hello')

            if (await WorkSpace.findOne({
                    _id: req.use.id,
                    name: req.body.name
                })) {
                return res.status(409).json({
                    error: "A workspace with the same name already exists",
                    value: req.body.name
                })
            }

            let workspace = new WorkSpace({
                name: req.body.name,
                owner: req.user.id
            })

            workspace = await workspace.save()


            res.status(201).json({
                success: workspace
            })


        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }
    },

    getWorkSpaces(owned = false, memberOf = true) {
        return async (req, res) => {
            try {

                let workspaces;

                if (owned && memberOf) {
                    workspaces = await WorkSpace.find({
                        $or: [{
                            owner: req.user.id
                        }, {
                            users: req.user.id
                        }]
                    });
                } else if (owned) {
                    workspaces = await WorkSpace.find({
                        owner: req.user.id
                    });
                } else {
                    workspaces = await WorkSpace.find({
                        users: req.user.id
                    });
                }

                res.status(200).json({
                    success: workspaces
                })


            } catch (error) {
                res.status(500).json({
                    error: error.toString()
                })
            }
        }
    },

    async getWorkSpace(req, res) {

        try {

            let workspace = req.workspace


            if (workspace.owner === req.user.id || workspace.users.includes(req.user.id)) {
                return res.status(200).json({
                    success: workspace
                })
            }

            workspace = await WorkSpace.populate(workspace, {
                path: 'users'
            })

            res.status(403).json({
                error: "Permission Denied"
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }

    },

    async updateWorkSpace(req, res) {
        try {

            const {
                name
            } = req.body

            let workspace = req.workspace

            if (await WorkSpace.findOne({
                    name,
                    $or: [{
                            owner: req.user.id
                        },
                        {
                            users: req.user.id
                        }
                    ]
                })) {

                return res.status(409).json({
                    error: {
                        message: "A workspace with the same name already exists"
                    }
                })
            }

            if (workspace.owner.toString() === req.user.id) {
                workspace.name = name
                workspace = await workspace.save()

                return res.status(200).json({
                    success: {
                        workspace
                    }
                })

            }

            res.status(403).json({
                error: "Permission Denied"
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }
    },

    async deleteWorkSpace(req, res) {

        try {

            let workspace = req.workspace
            let passcode = req.body.passcode

            console.log(workspace.owner, req.user.id)
            if (workspace.owner.toString() === req.user.id) {

                if (passcode === workspace._id.toString()) {
                    await WorkSpace.deleteOne({
                        _id: workspace._id
                    })


                    return res.status(200).json({
                        success: workspace,
                        deleted: "true"
                    })
                } else {
                    return res.status(206).json({
                        success: {
                            message: "Send this key to continue with deleting this workspace as passcode",
                            passcode: req.workspace._id
                        }
                    })
                }
            }



            res.status(403).json({
                error: "Permission Denied"
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }

    },

    async addMembers(req, res) {
        try {
            const workspaceId = req.workspace._id
            const {

                newMemberIds
            } = req.body

            const workspace = await WorkSpace.findOne({
                _id: workspaceId,
                owner: req.user.id
            })

            if (!workspace) {
                return res.status(404).json({
                    error: "No such workspace belongs to you."
                })
            }

            const users = await User.find({
                _id: newMemberIds
            })

            if (users)
                if (users.length === 0)
                    return res.status(404).json({
                        error: `No such user${newMemberIds.length>1?'s':''} exist${newMemberIds.length>1?'':'s'}`
                    })

            console.log(workspace)

            workspace.users.push(users.map((user) => user._id))
            workspace.users = workspace.users.filter((item, index) => workspace.users.indexOf(item) === index)
            workspace.save()

            res.status(200).json({
                success: workspace
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }
    },

    async deleteMember(req, res) {
        try {

            const {
                workspaceId,
                newMemberId
            } = req.body

            let workspace = await WorkSpace.findOne({
                _id: workspaceId,
                users: newMemberId
            })

            if (!workspace) {
                return res.status(404).json({
                    error: "No such workspace belongs to you."
                })
            }

            const users = await User.find({
                _id: newMemberIds
            })

            if (users)
                if (users.length === 0)
                    return res.status(404).json({
                        error: `No such user${newMemberIds.length>1?'s':''} exist${newMemberIds.length>1?'':'s'}`
                    })

            console.log(workspace)

            workspace.users.push(users.map((user) => user._id))
            workspace.users = workspace.users.filter((item, index) => workspace.users.indexOf(item) === index)
            workspace.save()

            res.status(200).json({
                success: workspace
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }
    },

    async getAllMembers(req, res) {

        try {

            const workspaceId = req.params.workspaceId

            let workspace = await WorkSpace.findOne({
                _id: workspaceId
            })

            if (!workspace) {
                return res.status(404).json({
                    error: "No such workspace exists."
                })
            }

            workspace = await WorkSpace.populate(workspace, [{
                path: 'users'
            }, {
                path: 'owner'
            }])

            res.status(200).json({
                success: {
                    owner: workspace.owner,
                    users: workspace.users
                }
            })

        } catch (error) {
            res.status(500).json({
                error: error.toString()
            })
        }
    }
};