const WorkSpace = require("../models/WorkSpace");
const User = require("../models/User");
const Chat = require("../models/Chat");
const Channel = require("../models/Channel");
const { json } = require("express");

module.exports = {
  async getDashBoardData(req, res) {
    const userId = req.user.id;

    try {
      const workspaces = await WorkSpace.find({
        $or: [{ owner: userId }, { users: userId }],
      });

      const user = await User.findOne({ _id: userId });

      const channels = await Channel.find({ users: userId });

      const chats = await Chat.find({ users: userId });

      res.status(200).
        json({
          user,
          workspaces,
          chats,
          channels,
        });
    } catch (err) {
      res.status(500).json({ error: err.toString() });
    }
  },
};
