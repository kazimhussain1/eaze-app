let data = {
    TIME_SERIES_CONFIRMED: 'confirmed_cases',
    TIME_SERIES_DEATHS: 'deaths',
    TIME_SERIES_RECOVERED: 'recovered'
}

module.exports = data