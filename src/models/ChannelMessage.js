const mongoose = require("mongoose");

const channelMessageSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  channelId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Channel",
    required: true
  },
  message: {
    type: String,
    required: false
  },
  mediaType: {
    type: String,
    required: false
  },
  media: {
    type: String,
    required: false
  }
});

module.exports = mongoose.model("ChannelMessage", channelMessageSchema);
