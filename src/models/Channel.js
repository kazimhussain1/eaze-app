const mongoose = require('mongoose')

const channelSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    users: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    ]
})

const Channel = module.exports = mongoose.model('Channel', channelSchema)

module.exports.getChannels = (callback, limit) => {
	Channel.find(callback).limit(limit);
}

// Get Channel
module.exports.getChannelById = (id, callback) => {
	Channel.findById(id, callback);
}

// Add Channel
module.exports.addChannel = (book, callback) => {
	Channel.create(book, callback);
}

// Update Channel
module.exports.updateChannel = (id, book, options, callback) => {
	var query = {_id: id};
	var update = {
		name: book.name,
		users: book.users,
	}
	Channel.findOneAndUpdate(query, update, options, callback);
}

// Delete Channel
module.exports.removeChannel = (id, callback) => {
	var query = {_id: id};
	Channel.remove(query, callback);
}