const mongoose = require("mongoose");

const mediaSchema = new mongoose.Schema({
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }]
});

module.exports = mongoose.model("Media", mediaSchema);

