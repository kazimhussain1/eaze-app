const mongoose = require("mongoose");

const workSpaceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: false,
    default: []
  }],
  channels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Channel",
    required: false,
    default: []
  }]
},{versionKey: false, timestamps:true});

module.exports = mongoose.model("WorkSpace", workSpaceSchema);