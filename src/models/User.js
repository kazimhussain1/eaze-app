const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    verified: {
      type: Boolean,
      required: false,
      default: false,
    },
    workSpaces: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "WorkSpace",
        required: false,
      },
    ],
    profilePhotoUrl: {
      type: String,
      required: false,
      default: "NO PHOTO",
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);


const User = module.exports = mongoose.model("User", userSchema);


module.exports.getUsers = (callback, limit) => {
  User.find(callback).limit(limit);
};

// Get User
module.exports.getUserById = (id, callback) => {
  User.findById(id, callback);
};

module.exports.getUserByEmailOrUsername = (email, username, callback) => {
  User.findOne()
    .or([
      {
        email: email,
      },
      {
        username: username,
      },
    ])
    .exec(callback);
};

// Add User
module.exports.addUser = (user, callback) => {
  User.create(user, callback);
};

// Update User
module.exports.updateUser = (id, user, options, callback) => {
  var query = {
    _id: id,
  };
  var update = {
    firstName: user.firstName,
    lastName: user.lastName,
    username: user.username,
  };
  User.findOneAndUpdate(query, update, options, callback);
};

// Delete User
module.exports.removeUser = (id, callback) => {
  var query = {
    _id: id,
  };
  User.remove(query, callback);
};
