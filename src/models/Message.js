const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  chatId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Chat",
    required: true
  },
  message: {
    type: String,
    required: false
  },
  mediaType: {
    type: String,
    required: false
  },
  media: {
    type: String,
    required: false
  }
}, {
  timestamps: true,
  versionKey: false,
});

module.exports = mongoose.model("Message", messageSchema);
