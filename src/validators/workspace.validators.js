const {
    body,
    param
} = require('express-validator')
const errorHandler = require('./errorHandler')
const WorkSpace = require('../models/WorkSpace')

const config = require('../config')
const idRegex = new RegExp(`^[0-9A-Fa-f]{${config.DB_ID_LENGTH}}$`)


async function checkWorkspace(req, res, next) {
    const workspaceId = req.body.workspaceId || req.params.workspaceId

    const workspace = await WorkSpace.findOne({
        _id: workspaceId
    })

    if (workspace) {
        req.workspace = workspace
        next()
    } else {
        res.status(404).json({
            error: "No such workspace exists."
        })
    }
}

function validateWorkSpaceId() {
    return [

        param("workspaceId")
        .exists()
        .matches(idRegex).withMessage(
            `'workspaceId' should be a hex string of length ${config.DB_ID_LENGTH}.`,
        ),

        errorHandler,

        checkWorkspace
    ]
}

module.exports = {
    validateCreation: () => [
        body('name')
        .exists()
        .withMessage("'name' is required")
        .isString(),

        errorHandler
    ],

    validateAddMembers: () => [
        body('newMemberIds')
        .exists()
        .withMessage(
            `'newMemberIds' should should only contain hex strings of length ${config.DB_ID_LENGTH}.`,
        )
        .isArray({
            min: 1
        })
        .custom((value) => {
            if (value instanceof Array) {
                for (let i = 0; i < value.length; i++) {
                    if (!idRegex.test(value[i]))
                        return false

                }
            }

            return true
        }),

        ...validateWorkSpaceId()
    ],

    validateWorkSpaceId


}