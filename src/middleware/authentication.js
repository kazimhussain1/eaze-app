const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    // Get token from header
    let token = req.header('authorization');
    // check if not token
    if (!token) {
        return res.status(401).json({
            message: 'No token, authorization denied'
        });
    }

    // Verify token
    schemePlusToken = token.split(' ');

    try {
        if (schemePlusToken[0] = 'Bearer') {

            token = schemePlusToken[1]
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            req.user = decoded.user;
            next();

        } else {
            res.status(401).json({
                message: 'Token is not valid'
            });
        }

    } catch (err) {
        console.log(err)
        res.status(401).json({
            message: 'Token is not valid'
        });
    }
}