const router = require('express').Router();
const channelRepo = require('../repositories/channel.repo')

router.post("/", channelRepo.createChannel)

module.exports = router