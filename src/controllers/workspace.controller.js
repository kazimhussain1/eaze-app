const router = require('express').Router();
const workSpaceValidators = require('../validators/workspace.validators')
const workspaceRepo = require('../repositories/workspace.repo')


router.post('/', workSpaceValidators.validateCreation(), workspaceRepo.createWorkSpace)
router.get('/all', workspaceRepo.getWorkSpaces(true, true))
router.get('/owned', workspaceRepo.getWorkSpaces(true, false))
router.get('/memberOf', workspaceRepo.getWorkSpaces(false, true))

router.get('/:workspaceId', workSpaceValidators.validateWorkSpaceId(), workspaceRepo.getWorkSpace)
router.put('/:workspaceId', workSpaceValidators.validateWorkSpaceId(), workspaceRepo.updateWorkSpace)
router.delete('/:workspaceId', workSpaceValidators.validateWorkSpaceId(), workspaceRepo.deleteWorkSpace)
router.put('/:workspaceId/addMembers', workSpaceValidators.validateAddMembers(), workspaceRepo.addMembers)
router.get('/:workspaceId/allMembers', workSpaceValidators.validateWorkSpaceId(), workspaceRepo.getAllMembers)

module.exports = router;