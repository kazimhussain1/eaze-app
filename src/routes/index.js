const router = require("express").Router();
const authentication = require("../middleware/authentication");
const user = require("../controllers/user.controller");
const workspace = require("../controllers/workspace.controller");
const dashboard = require("../controllers/dashboard.controller");
const chat = require("../controllers/chat.controller");
const channel = require("../controllers/channel.controller");

router.use("/user", user);

router.use(authentication);

router.use("/workspace", workspace);
router.use("/dashboard", dashboard);
router.use("/chat", chat);
router.use("/channel", channel);

module.exports = router;
