## Eaze brings the team together, wherever you are:
Eaze is a channel-based messaging platform. With Eaze, people can work together more effectively within a secure environment.
![](eaze.gif)
# Steps To Run Eaze:
1. Install node modules(back-end)
    * <path>App\eaze-app> npm install all
2. Navigate to client director and again install node modules(front-end)
    * <path>App\eaze-app> cd client
    * <path>App\eaze-app\client> npm install all
3. Move to root director
    * <path>App\eaze-app\client> cd ..
4. Run Eaze
    * <path>App\eaze-app> npm run app
    
## Requirements:
    MongoDB Server
    ### Follow the below steps to MongoDB Server:
    1. Go to Control Panel and click on Administrative Tools.
    2. Double click on Services. A new window opens up.
    3. Search MongoDB.exe. Right click on it and select Start.
    3. The server will start. Now execute npm run app again and the code might work this time.
